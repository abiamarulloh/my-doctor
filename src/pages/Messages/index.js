import React from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import {DoctorPict1, DoctorPict2, DoctorPict3} from '../../assets';
import {Gap} from '../../components';
import ListDoctorMessages from '../../components/molecules/ListDoctorMessages';
import {colors, fonts} from '../../utils';

const Messages = () => {
  return (
    <View style={styles.page}>
      <View style={styles.container}>
        <ScrollView>
          <Text style={styles.messagesHeadTitle}>Messages</Text>
          <Gap height={16} />
          <ListDoctorMessages
            imgUrl={DoctorPict1}
            name="Alexander Jannie"
            shortChat="Baik ibu, terima kasih banyak atas wakt..."
          />
          <Gap height={16} />
          <ListDoctorMessages
            imgUrl={DoctorPict2}
            name="Nairobi Putri Hayza"
            shortChat="Oh tentu saja tidak karena jeruk it..."
          />
          <Gap height={16} />

          <ListDoctorMessages
            imgUrl={DoctorPict3}
            name="John McParker Steve"
            shortChat="Oke menurut pak dokter bagaimana unt..."
          />
        </ScrollView>
      </View>
    </View>
  );
};

export default Messages;
const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.secondary,
    flex: 1,
  },
  container: {
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 30,
    backgroundColor: 'white',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
  },
  messagesHeadTitle: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
  },
});
