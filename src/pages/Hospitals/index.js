import React from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import ImageBackground from 'react-native/Libraries/Image/ImageBackground';
import {
  BgHospitalDummy,
  HospitalPict1,
  HospitalPict2,
  HospitalPict3,
} from '../../assets';
import ListHospitals from '../../components/molecules/ListHospitals';
import {colors, fonts} from '../../utils';

const Hospitals = () => {
  return (
    <View style={styles.page}>
      <ImageBackground source={BgHospitalDummy} style={styles.bgImage}>
        <View style={styles.wrapTextHead}>
          <Text style={styles.textTitleHospitals}>Nearby Hospitals</Text>
          <Text style={styles.textAmountHospitals}>3 tersedia</Text>
        </View>
      </ImageBackground>
      <View style={styles.content}>
        <ListHospitals
          imgUrl={HospitalPict1}
          title="Rumah Sakit Citra Bunga Merdeka"
          address="Jln. Surya Sejahtera 20"
        />
        <ListHospitals
          imgUrl={HospitalPict2}
          title="Rumah Sakit Anak Happy Family & Kids"
          address="Jln. Surya Sejahtera 20"
        />
        <ListHospitals
          imgUrl={HospitalPict3}
          title="Rumah Sakit Jiwa Tingkatan Paling Atas"
          address="Jln. Surya Sejahtera 20"
        />
      </View>
    </View>
  );
};

export default Hospitals;
const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.secondary,
    flex: 1,
  },
  container: {
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 30,
    backgroundColor: 'white',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
  },
  bgImage: {
    height: 240,
  },
  textTitleHospitals: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: 'white',
  },
  textAmountHospitals: {
    fontSize: 14,
    fontFamily: fonts.primary[400],
    color: 'white',
  },
  wrapTextHead: {
    paddingVertical: 30,
    alignItems: 'center',
  },
  content: {
    backgroundColor: 'white',
    borderRadius: 24,
    flex: 1,
    paddingVertical: 30,
    paddingHorizontal: 16,
    marginTop: -30,
  },
});
