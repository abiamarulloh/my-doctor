import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {
  DoctorPict1,
  DoctorPict2,
  DoctorPict3,
  NewsPict1,
  NewsPict2,
  NewsPict3,
} from '../../assets';
import {DoctorCategory, Gap, HomeProfile} from '../../components';
import DoctorRated from '../../components/molecules/DoctorRated';
import GoodNews from '../../components/molecules/GoodNews';
import {colors, fonts} from '../../utils';

const Doctor = () => {
  return (
    <View style={styles.page}>
      <View style={styles.container}>
        <ScrollView>
          <HomeProfile />

          <Gap height={30} />

          <View>
            <View>
              <Text style={styles.titleDoctorCategory}>
                Mau konsultasi dengan siapa hari ini?
              </Text>
              <Gap height={16} />
              <ScrollView horizontal={true}>
                <View style={{flexDirection: 'row'}}>
                  <DoctorCategory profession="dokter umum" />
                  <Gap width={10} />
                  <DoctorCategory profession="psikiater" />
                  <Gap width={10} />
                  <DoctorCategory profession="dokter obat" />
                  <Gap width={10} />
                  <DoctorCategory profession="dokter anak" />
                </View>
              </ScrollView>
            </View>

            <Gap height={30} />

            <View style={styles.doctorRated}>
              <Text style={styles.title}>Top Rated Doctors</Text>
              <Gap height={16} />
              <View>
                <DoctorRated
                  imgUrl={DoctorPict1}
                  name="Alexa Rachel"
                  specialist="Pediatrician"
                />
                <Gap height={16} />

                <DoctorRated
                  imgUrl={DoctorPict2}
                  name="Sunny Frank"
                  specialist="Dentist"
                />
                <Gap height={16} />

                <DoctorRated
                  imgUrl={DoctorPict3}
                  name="Poe Minn"
                  specialist="Podiatrist"
                />
              </View>
            </View>

            <Gap height={30} />

            <View style={styles.goodNews}>
              <Text style={styles.title}>Good News</Text>
              <Gap height={16} />

              <GoodNews
                imgUrl={NewsPict1}
                title="Is it safe to stay at home during coronavirus?"
                date="Today"
              />
              <Gap height={16} />

              <GoodNews
                imgUrl={NewsPict2}
                title="Consume yellow citrus helps you healthier"
                date="Today"
              />
              <Gap height={16} />

              <GoodNews
                imgUrl={NewsPict3}
                title="Learn how to make a proper orange juice at home"
                date="Today"
              />
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default Doctor;
const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.secondary,
    flex: 1,
  },
  container: {
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 30,
    backgroundColor: 'white',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
  },
  titleDoctorCategory: {
    fontSize: 20,
    width: 209,
    height: 48,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
  },
  title: {
    fontSize: 16,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
  },
});
