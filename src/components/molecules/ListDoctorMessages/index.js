import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

import {colors, fonts} from '../../../utils';

const ListDoctorMessages = ({imgUrl, name, shortChat, countStars = 5}) => {
  return (
    <TouchableOpacity style={styles.container}>
      <Image source={imgUrl} style={styles.profilePicture} />
      <View>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.shortChat}>{shortChat}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default ListDoctorMessages;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 62,
    lineHeight: 62,
    width: '100%',
    borderBottomWidth: 1,
    borderColor: colors.borderSecondary,
  },
  profilePicture: {
    width: 46,
    height: 46,
    borderRadius: 100,
    marginRight: 12,
  },
  name: {
    fontSize: 16,
    color: colors.text.primary,
    fontFamily: fonts.primary[600],
  },
  shortChat: {
    fontSize: 12,
    color: colors.text.menuInactive,
    fontFamily: fonts.primary[400],
  },
});
