import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {UserPictureDummy} from '../../../assets';
import {colors, fonts} from '../../../utils';

const HomeProfile = ({}) => {
  return (
    <TouchableOpacity style={styles.container}>
      <Image source={UserPictureDummy} style={styles.profilePicture} />
      <View style={styles.profile}>
        <Text style={styles.title}>Shayna Melinda</Text>
        <Text style={styles.profession}>Product Designer</Text>
      </View>
    </TouchableOpacity>
  );
};

export default HomeProfile;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  profilePicture: {
    width: 46,
    height: 46,
    borderRadius: 100,
  },
  profile: {
    marginLeft: 12,
  },
  title: {
    fontSize: 16,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
  },
  profession: {
    fontSize: 12,
    fontFamily: fonts.primary[400],
    color: colors.text.secondary,
  },
});
