import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {
  IDokterAnak,
  IDokterObat,
  IDokterUmum,
  IPsikiater,
} from '../../../assets';
import {colors, fonts} from '../../../utils';
import {TouchableOpacity} from 'react-native-gesture-handler';

const DoctorCategory = ({profession}) => {
  const Icon = () => {
    if (profession === 'dokter umum') {
      return <IDokterUmum />;
    }

    if (profession === 'psikiater') {
      return <IPsikiater />;
    }

    if (profession === 'dokter obat') {
      return <IDokterObat />;
    }

    if (profession === 'doctor anak') {
      return <IDokterAnak />;
    }

    return <IDokterUmum />;
  };

  return (
    <TouchableOpacity style={styles.container}>
      <Icon style={styles.icon} />
      <View style={styles.descIcon}>
        <Text style={styles.title}>Saya Butuh</Text>
        <Text style={styles.profession}>{profession}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default DoctorCategory;
const styles = StyleSheet.create({
  container: {
    width: 100,
    height: 130,
    backgroundColor: colors.lightGreen,
    paddingVertical: 12,
    paddingHorizontal: 12,
    borderRadius: 10,
  },
  descIcon: {
    marginTop: 28,
  },
  profilePicture: {
    width: 46,
    height: 46,
    borderRadius: 100,
  },
  title: {
    fontSize: 12,
    color: colors.text.primary,
    fontFamily: fonts.primary[300],
  },
  profession: {
    fontSize: 12,
    color: colors.text.menuInactive,
    fontFamily: fonts.primary[600],
  },
});
