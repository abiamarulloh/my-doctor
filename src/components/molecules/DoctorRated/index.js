import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {IStarActive} from '../../../assets';
import {colors, fonts} from '../../../utils';

const DoctorRated = ({imgUrl, name, specialist, countStars = 5}) => {
  return (
    <TouchableOpacity style={styles.container}>
      <View style={styles.profile}>
        <Image source={imgUrl} style={styles.profilePicture} />
        <View style={styles.descIcon}>
          <Text style={styles.title}>{name}</Text>
          <Text style={styles.profession}>{specialist}</Text>
        </View>
      </View>
      <View style={styles.ratedStar}>
        <IStarActive styles={styles.ratedStarItem} />
        <IStarActive styles={styles.ratedStarItem} />
        <IStarActive styles={styles.ratedStarItem} />
        <IStarActive styles={styles.ratedStarItem} />
        <IStarActive styles={styles.ratedStarItem} />
      </View>
    </TouchableOpacity>
  );
};

export default DoctorRated;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  descIcon: {
    marginLeft: 12,
  },
  profile: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  profilePicture: {
    width: 50,
    height: 50,
    borderRadius: 100,
  },
  title: {
    fontSize: 16,
    color: colors.text.primary,
    fontFamily: fonts.primary[600],
  },
  profession: {
    fontSize: 12,
    color: colors.text.primary,
    fontFamily: fonts.primary[400],
  },
  ratedStar: {
    flexDirection: 'row',
  },
  ratedStarItem: {
    width: 24,
    height: 24,
  },
});
