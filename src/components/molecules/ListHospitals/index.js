import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

import {colors, fonts} from '../../../utils';

const ListHospitals = ({imgUrl, title, address, countStars = 5}) => {
  return (
    <TouchableOpacity style={styles.container}>
      <Image source={imgUrl} style={styles.profilePicture} />
      <View>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.address}>{address}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default ListHospitals;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 76,
    lineHeight: 76,
    width: '100%',
    borderBottomWidth: 1,
    borderColor: colors.borderSecondary,
  },
  profilePicture: {
    width: 80,
    height: 60,
    marginRight: 12,
  },
  title: {
    fontSize: 16,
    color: colors.text.primary,
    fontFamily: fonts.primary[400],
  },
  address: {
    fontSize: 12,
    color: colors.text.menuInactive,
    fontFamily: fonts.primary[300],
  },
});
