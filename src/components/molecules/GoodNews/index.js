import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';
import {TouchableOpacity} from 'react-native-gesture-handler';

const GoodNews = ({imgUrl, title, date}) => {
  return (
    <TouchableOpacity style={styles.container}>
      <View style={styles.profile}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.date}>{date}</Text>
      </View>
      <Image source={imgUrl} style={styles.newsPicture} />
    </TouchableOpacity>
  );
};

export default GoodNews;
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 76,
    borderBottomColor: colors.borderSecondary,
    borderBottomWidth: 1,
  },
  newsPicture: {
    width: 80,
    height: 60,
    borderRadius: 11,
  },
  title: {
    fontSize: 16,
    width: 204,
    color: colors.text.primary,
    fontFamily: fonts.primary[600],
  },
  date: {
    fontSize: 12,
    color: colors.text.menuInactive,
    fontFamily: fonts.primary[400],
  },
});
