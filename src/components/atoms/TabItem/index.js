import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {
  IconDoctor,
  IconDoctorActive,
  IconHospital,
  IconHospitalActive,
  IconMessages,
  IconMessagesActive,
} from '../../../assets';
import {colors, fonts} from '../../../utils';

const TabItem = ({title, isActive, onPress, onLongPress}) => {
  const Icon = () => {
    if (title === 'Doctor') {
      return isActive ? <IconDoctorActive /> : <IconDoctor />;
    }

    if (title === 'Messages') {
      return isActive ? <IconMessagesActive /> : <IconMessages />;
    }

    if (title === 'Hospital') {
      return isActive ? <IconHospitalActive /> : <IconHospital />;
    }

    return <IconDoctor />;
  };
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={onPress}
      onLongPress={onLongPress}>
      <Icon />
      <Text style={styles.title(isActive)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default TabItem;
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  title: isActive => ({
    color: isActive ? colors.text.menuActive : colors.text.menuInactive,
    marginTop: 4,
    fontFamily: fonts.primary[600],
    fontSize: 10,
  }),
});
