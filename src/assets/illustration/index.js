import ILLogo from './logo.svg';
import IGetStarted from './get-started.png';
import IconBackDark from './arrow-left.svg';
import ILNullPhoto from './null-photo.png';
import IDokterUmum from './ic-dokter-umum.svg';
import IDokterAnak from './ic-dokter-anak.svg';
import IPsikiater from './ic-psikiater.svg';
import IDokterObat from './ic-dokter-obat.svg';
import IStarActive from './ic-star-active.svg';

export {
  ILLogo,
  IGetStarted,
  IconBackDark,
  ILNullPhoto,
  IDokterUmum,
  IPsikiater,
  IDokterObat,
  IDokterAnak,
  IStarActive,
};
