import UserPictureDummy from './user-pic-dummy.png';
import DoctorPict1 from './doc-pic-1.png';
import DoctorPict2 from './doc-pic-2.png';
import DoctorPict3 from './doc-pic-3.png';
import HospitalPict1 from './hospital-pic-1.png';
import HospitalPict2 from './hospital-pic-2.png';
import HospitalPict3 from './hospital-pic-3.png';
import NewsPict1 from './news-pic-1.png';
import NewsPict2 from './news-pic-2.png';
import NewsPict3 from './news-pic-3.png';
import BgHospitalDummy from './bg-hospital-dummy.png';

export {
  UserPictureDummy,
  DoctorPict1,
  DoctorPict2,
  DoctorPict3,
  HospitalPict1,
  HospitalPict2,
  HospitalPict3,
  NewsPict1,
  NewsPict2,
  NewsPict3,
  BgHospitalDummy,
};
