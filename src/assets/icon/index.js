import IconAddPhoto from './ic-add-photo.svg';
import IconRemovePhoto from './ic-remove-photo.svg';
import IconDoctor from './ic-doctor.svg';
import IconDoctorActive from './ic-doctor-active.svg';
import IconMessages from './ic-messages.svg';
import IconMessagesActive from './ic-messages-active.svg';
import IconHospital from './ic-hospital.svg';
import IconHospitalActive from './ic-hospital-active.svg';

export {
  IconAddPhoto,
  IconRemovePhoto,
  IconDoctor,
  IconMessages,
  IconMessagesActive,
  IconDoctorActive,
  IconHospital,
  IconHospitalActive,
};
